
/**
* Wrapper class around a Talon SRX
*/
class CustomSRX(id: Int) : TalonSRX(id) {
  /**
  * Property around a velocity control loop on a talon
  */
  var speed: Double
    get() = this.getSelectedSensorVelocity(0)
    set(value) {
      this.set(ControlMode.Velocity, value)
    }
}

fun main(args: Array<String>) {
  val customSRX = CustomSRX(1)
  customSRX.speed = 150 // Sets the closed loop set point to 150 sensor units/100ms
  ...
 
  val velocity = customSRX.speed // Calls the custom getter (Gets the velocity from the sensor)
}
