
fun main(args: Array<String>) {
    // [str] is abstracted behind the Lazy delegate.
    val str by lazy {
        // This code will be run the first time the property is accessed, afterwards the returned value will be given immediately
        Thread.sleep(5000) // Simulate expensive computation
        "Hello, World!"
    }

    println(str) // This operation will be slowed by 5 seconds as the value is computed for the first time
    println(str) // This operation will be immediate. The value returned the first time is given again
}
