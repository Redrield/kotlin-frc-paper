import kotlinx.coroutines.experimental.*

fun main(args: Array<String>) {
    launch {
      // suspend while asynchronously reading
      val bytesRead = inChannel.aRead(buf)
      // we only get to this line when reading completes
          ...
          ...
      process(buf, bytesRead)
      // suspend while asynchronously writing
      outChannel.aWrite(buf)
      // we only get to this line when writing completes
          ...
          ...
      outFile.close()
    }
}
