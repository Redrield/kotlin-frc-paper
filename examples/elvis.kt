
fun main(args: Array<String>) {
  val i: Int? = 4
  println(takesANullable(i)) // Will print 5
  val j: Int? = null
  println(takesANullable(j)) // Will print -1

  val s = j ?: 5 // Will contain 5
}

fun takesANullable(i: Int?): Int {
  val iUnwrapped = i ?: return -1 // If i is not null, the inner value will be bound in iUnwrapped. If it's null the function will return -1

  return iUnwrapped + 1
}
