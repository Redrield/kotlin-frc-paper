
public class StringUtils {
    /**
     * Capitalizes the first letter of a String
     * Usage: StringUtils.capitalize("hello")
    */
    public static String capitalize(String str) {
        if(str.isEmpty()) {
            return "";
        }

        char[] arr = str.toCharArray();
        if(Character.isLowerCase(arr[0])) {
            return str.substring(0, 1).toUpperCase() + str.substring(1);
        }else {
            return str;
        }
    }
}
