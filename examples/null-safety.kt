
fun main(args: Array<String>) {
  var s: String = "Hello" // s can never be null
  s = null // Compile time error

  val i: Int = null // Compile time error

  val j: List<String>? = null // Ok, marked as nullable
  val s: String? = j?.get(0) // Safe access on j
}
