
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class NullAnnotations {

    /**
     * Returns the literal String "Hello, World!"
     * Return type in Kotlin is `String` (Marked with JSR-305 annotation).
    */
    @Nonnull
    public String nonnullString() {
        return "Hello, World!"; 
    }


    /**
     * Returns null
     * Return type in Kotlin is `String?`
    */
    @Nullable
    public String nullableString() {
        return null;
    }

    /**
     * Returns the literal String "Hello"
     * Return type in Kotlin is `String!` (May or may not be nullable)
    */
    public String platformType() {
        return "Hello";
    }
}
