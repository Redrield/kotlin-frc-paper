/**
 * A class encapsulating two fields, with implementations of toString(), hashCode(), and equals()
*/
public class JavaPojo {
    private final String name;
    private int age;

    public JavaPojo(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "JavaPojo(name=" + this.name + ",age=" + this.age + ")";
    }

    @Override
    public int hashCode() {
        return 31 * this.age + this.name.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if(this == other) {
            return true;
        }
        if(other instanceof JavaPojo) {
            JavaPojo obj = (JavaPojo) other;
            return obj.name.equals(this.name) && obj.age == this.age;
        }else {
            return false;
        }
    }
}
