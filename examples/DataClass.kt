
/**
 * A Kotlin data class encapsulating two fields
 * [name] is an immutable property of type String (Cannot be reassigned)
 * [age] is a mutable property of type Int (Can be reassigned)
*/
data class Person(val name: String, var age: Int)
